extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;
#[macro_use]
extern crate glium;
extern crate rand;
#[macro_use]
extern crate log;

extern crate piston_window;
use piston_window::*;

use std::fs::File;
use std::io::Read;
use rand::Rng;

extern crate float;
use float::*;
use std::env;
use std::path::Path;

struct CPU{
    gfx : Vec<u8>,
    pc : usize,
    stack : Vec<u16>,
    sp: usize,
    opcode : u16,
    register: Vec<u8>,
    I : u16,
    keys : Vec<u8>,
    dt : u8,
    st : u8
}

impl CPU{
    fn new() -> CPU{
        let mut cpu = CPU {
            gfx : Vec::with_capacity(64*32),
            pc :  0x200,
            stack : Vec::with_capacity(16),
            sp: 0,
            opcode : 0,
            register: Vec::with_capacity(16),
            I : 0,
            keys : Vec::with_capacity(16), // 0-F
            dt :  0,
            st :  0
        };
        for i in (0..16){
            cpu.register.push(0);
            cpu.keys.push(0);
            cpu.stack.push(0);
        }

        for i in (0..2048){
            cpu.gfx.push(0);
        }
        
        cpu
    }

    fn reset(&mut self){
        self.pc = 512;
        self.sp = 0;
        for i in (0..16){
            self.register[i] = 0;
            self.keys[i] = 0;
            self.stack[i] = 0;
        }

        for i in (0..(64*32)){
            self.gfx.push(0);
        }
        self.dt = 0;
        self.st = 0;
    }

    fn execute(&mut self, memory:&mut Vec<u8>) -> bool {
        let mut is_draw_needed = false;
        let mut opcode = 0;
        opcode = ((memory[self.pc] as u16) << 8) | memory[self.pc+1] as u16;
        
        //////println!("opcode is {}", opcode);
        ////println!("{}", (opcode & 0xF000) >> 12);
        
        match opcode & 0xF000{
            0x0000 => {
                ////println!("0");
                match opcode & 0x000F{
                    0x0000 => {
                        //println!("clearing the screen");
                        //target.clear_color(0.0,0.0,0.0,0.0);
                        is_draw_needed = true;
                        for i in (0..2048){
                            self.gfx[i] = 0;
                        }
                        self.pc += 2;
                    },
                    0x000E => {
                        //println!("returning from a subroutine");
                        self.sp -= 1;
                        self.pc = self.stack[self.sp] as usize;
                        self.pc += 2;
                    },
                    _ => {}
                };
            },
            0x1000 => {
                //println!("jumping to address {}", opcode & 0x0FFF);
                self.pc = (opcode & 0x0FFF) as usize;
            },
            0x2000 => {
                //println!("calling sub at address");
                self.stack[self.sp] = self.pc as u16;
                self.sp += 1;
                self.pc = (opcode & 0x0FFF) as usize;
            },
            0x3000 => {
                //println!("skipping next instr");
                if self.register[((opcode & 0x0F00) >> 8) as usize] == (opcode & 0x00FF) as u8 {
                    self.pc += 4;
                }
                else{
                    self.pc += 2;
                }
            },
            0x4000 => {
                //println!("skipping next instr");
                if self.register[((opcode & 0x0F00) >> 8) as usize] != (opcode & 0x00FF) as u8{
                    self.pc += 4;
                }
                else{
                    self.pc += 2;
                }
            },
            0x5000 => {
                //println!("skipping next instr");
                if self.register[((opcode & 0x0F00) >> 8) as usize] == self.register[((opcode & 0x00F0) >> 4) as usize]{
                    self.pc += 4;
                }
                else{
                    self.pc += 2;
                }
            },
            0x6000 => {
                //println!("set vx to nn");
                self.register[((opcode & 0x0F00) >> 8) as usize] = (opcode & 0x00FF) as u8;
                self.pc += 2;
            },
            0x7000 => {
                //println!("add nn to vx");
                //println!("register: {}", self.register[((opcode & 0x0F00) >> 8) as usize]);
                //println!("adding: {}", (opcode & 0x00FF));
                self.register[((opcode & 0x0F00) >> 8) as usize] = self.register[((opcode & 0x0F00) >> 8) as usize].wrapping_add((opcode & 0x00FF) as u8);
                self.pc += 2;
                //println!("done with 7");
            },
            0x8000 => {
                //println!("set vx to vy");
                match opcode & 0x000F{
                    0x0000 => { self.register[((opcode & 0x0F00) >> 8) as usize] = self.register[((opcode & 0x00F0) >> 4) as usize]; self.pc += 2; },
                    0x0001 => { self.register[((opcode & 0x0F00) >> 8) as usize] |= self.register[((opcode & 0x00F0) >> 4) as usize]; self.pc += 2; },
                    0x0002 => { self.register[((opcode & 0x0F00) >> 8) as usize] &= self.register[((opcode & 0x00F0) >> 4) as usize]; self.pc += 2; },
                    0x0003 => { self.register[((opcode & 0x0F00) >> 8) as usize] ^= self.register[((opcode & 0x00F0) >> 4) as usize]; self.pc += 2; },
                    0x0004 => { 
                        //println!("code {}", 4);
                        let r = self.register[((opcode & 0x0F00) >> 8) as usize] as u16 + self.register[((opcode & 0x00F0) >> 4) as usize] as u16; 
                        if r > 255 { self.register[15] = 1; }
                        self.register[((opcode & 0x0F00) >> 8) as usize] = r as u8;
                        
                        self.pc += 2; 
                    },
                    0x0005 => {
                        //println!("code {}", 0x5);
                        self.register[15] = if self.register[((opcode & 0x00F0) >> 4) as usize] > self.register[((opcode & 0x0F00) >> 8) as usize]{
                            0
                        }
                        else{
                            1
                        };
                        self.register[((opcode & 0x0F00) >> 8) as usize] = self.register[((opcode & 0x0F00) >> 8) as usize].wrapping_sub(self.register[((opcode & 0x00F0) >> 4) as usize]);
                        self.pc += 2;
                    },
                    0x0006 =>{
                        //println!("code {}", 0x6);
                        self.register[15] = self.register[((opcode & 0x0F00) >> 8) as usize] & 0x1;
                        self.register[((opcode & 0x0F00) >> 8) as usize] >>= 1;
                        self.pc += 2;
                    },
                    0x0007 => {
                        //println!("code {}", 0x7);
                        self.register[15] = if self.register[((opcode & 0x0F00) >> 8) as usize] > self.register[((opcode & 0x00F0) >> 4) as usize]{
                            0
                        }
                        else{
                            1
                        };
                        self.register[((opcode & 0x0F00) >> 8) as usize] = self.register[((opcode & 0x00F0) >> 4) as usize].wrapping_sub(self.register[((opcode & 0x0F00) >> 8) as usize]);
                        self.pc += 2;                    
                    },
                    0x000E => {
                        //println!("code {}", 0xE);
                        self.register[15] =  self.register[((opcode & 0x0F00) >> 8) as usize] >> 7;
                        self.register[((opcode & 0x0F00) >> 8) as usize] <<= 1;
                        self.pc += 2;
                    },
                    _ => {}
                }
                
            },
            0x9000 => {
                if self.register[((opcode & 0x0F00) >> 8) as usize] != self.register[((opcode & 0x00F0) >> 4) as usize]{
                    self.pc += 4;
                }
                else{
                    self.pc += 2;
                }
            },
            0xA000 => {
                self.I = opcode & 0x0FFF;
                self.pc += 2;
            },
            0xB000 => {
                self.pc = self.register[0] as usize + (opcode & 0x0FFF) as usize;
            },
            0xC000 => {
                let r = rand::random::<u8>() & (opcode & 0x00FF) as u8;
                //println!("generation rand - {}", r);
                
                self.register[((opcode & 0x0F00) >> 8) as usize]= r;
                self.pc += 2;
            },
            0xD000 => {
                //Draw sprites
                //println!("drawing sprite");
                let x = self.register[((opcode & 0x0F00) >> 8) as usize] as u32;
                let y = self.register[((opcode & 0x00F0) >> 4) as usize] as u32;
                let height = (opcode & 0x000F) as u32;
                //println!("height - {}", height);
                self.register[15] = 0;
                let mut pixel = 0;
                for yline in (0..height as u32){
                    pixel = memory[(self.I+yline as u16) as usize];
                    for xline in (0..8 as u32){
                        if (pixel & (0x80 >> xline)) != 0{
                            //println!("x - {}, xline - {}, y = {}, yline - {}", x, xline,  y, yline);
                            
                            if self.gfx[(x + xline + ((y+yline)*64)) as usize] == 1{
                                self.register[15] = 1;
                            }
                            self.gfx[(x + xline + ((y+yline)*64)) as usize] ^= 1;
                        }
                    }
                }
                
                is_draw_needed = true;
                self.pc += 2;
            },
            0xE000 => {
                //println!("14");
                match opcode & 0x00FF{
                    0x009E => {
                        //println!("0x009E");
                        //println!("index - {}", (opcode & 0x0F00) >> 8);
                        
                        //println!("register val - {}", self.register[((opcode & 0x0F00) >> 8) as usize]);
                        if self.keys[self.register[((opcode & 0x0F00) >> 8) as usize] as usize] != 0{
                            self.pc += 4;
                        }
                        else{
                            self.pc += 2;
                        }
                    }, 
                    0x00A1 => {
                        //println!("0x00A1");
                        //println!("index - {}", (opcode & 0x0F00) >> 8);
                        //println!("register val - {}", self.register[((opcode & 0x0F00) >> 8) as usize]);
                        if self.keys[self.register[((opcode & 0x0F00) >> 8) as usize] as usize] == 0{
                            self.pc += 4;
                        }
                        else{
                            self.pc += 2;
                        }                
                        //println!("14 done");
                    },
                    _ => {}
                }
            },
            0xF000 => {
                match opcode & 0x00FF{
                    0x0007 => {
                        self.register[((opcode & 0x0F00) >> 8) as usize] = self.dt;
                        self.pc += 2;
                    },
                    0x000A => {
                        let mut key_pressed = false;
                        for i in (0..16 as usize){
                            if self.keys[i] != 0{
                                self.register[((opcode & 0x0F00) >> 8) as usize] = i as u8;
                                key_pressed = true;
                            }
                        }
                        if key_pressed{
                            self.pc += 2;
                        }
                        
                    },
                    0x0015 => {
                        self.dt = self.register[((opcode & 0x0F00) >> 8) as usize];
                        self.pc += 2;
                    },
                    0x0018 => {
                        self.st = self.register[((opcode & 0x0F00) >> 8) as usize];
                        self.pc += 2;
                    },
                    0x001E => {
                        if self.I + self.register[((opcode & 0x0F00) >> 8) as usize] as u16 > 0xFFF{
                            self.register[15] = 1;
                        }
                        else{
                            self.register[15] = 0;
                        }
                        self.I += self.register[((opcode & 0x0F00) >> 8) as usize] as u16;
                        self.pc += 2;
                    },
                    0x0029 => {
                        self.I = self.register[((opcode & 0x0F00) >> 8) as usize] as u16 * 5;
                        self.pc += 2;
                    },
                    0x0033 => {
                        let mut v = self.register[((opcode & 0x0F00) >> 8) as usize];
                        memory[self.I as usize] = v % 10;
                        v = v / 10;
                        memory[(self.I + 1) as usize] = v % 10;
                        v = v / 10;
                        memory[(self.I + 2) as usize] = v % 10;
                        self.pc += 2;
                    },
                    0x0055 => {
                        //println!("mem ld");
                        for i in (0..((opcode & 0x0F00) >> 8) + 1){
                            memory[(self.I + i) as usize] = self.register[i as usize];
                        }
                        self.I += ((opcode & 0x0F00) >> 8) + 1;
                        self.pc += 2;
                    },
                    0x0065 => {
                        //println!("reg ld");
                        for i in (0..((opcode & 0x0F00) >> 8) + 1){
                            self.register[i as usize] = memory[(self.I + i) as usize];
                            //println!("I - {}", self.I);
                            //println!("loading - {}", memory[(self.I) as usize]);
                        }
                        //println!("done reg ld");
                        self.I += ((opcode & 0x0F00) >> 8) + 1;
                        self.pc += 2;
                    },
                    _ => {}
                }
            },
            _ => {panic!("invalid instructiion - {}", opcode);}
        }

        if self.dt > 0{
            self.dt -= 1;
        }

        is_draw_needed
    }
}


fn main() {
    
    let mut cpu = CPU::new();
    let mut memory : Vec<u8> = Vec::with_capacity(4096);
    //this is important because capacity != len. 
    for i in (0..4096){
        memory.push(0);
    }

    let char_set = [
        0xF0, 0x90, 0x90, 0x90, 0xF0, //0
    0x20, 0x60, 0x20, 0x20, 0x70, //1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
    0x90, 0x90, 0xF0, 0x10, 0x10, //4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
    0xF0, 0x10, 0x20, 0x40, 0x40, //7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
    0xF0, 0x90, 0xF0, 0x90, 0x90, //A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
    0xF0, 0x80, 0x80, 0x80, 0xF0, //C
    0xE0, 0x90, 0x90, 0x90, 0xE0, //D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
    0xF0, 0x80, 0xF0, 0x80, 0x80  //F
        ];
    for i in (0..80){
       memory[i] = char_set[i];
    }

    let args : Vec<String> = env::args().collect();
    println!("{}", args[1]);
    let mut f = match File::open(/*"F:\\code\\rust\\piston-test\\target\\debug\\invaders.c8"*/ &Path::new(&args[1])){
        Ok(f) => f,
        Err(e) => panic!("error - {}", e)
    };

    let mut program = Vec::new();
    f.read_to_end(&mut program);

    let mut i = 512;
    for b in &program{
        memory[i] = *b;
        i += 1;
    }

    let window: PistonWindow = 
        WindowSettings::new("crust8", [640, 320])
        .exit_on_esc(true).into();
    let black = [0.0, 0.0, 0.0, 1.0];
    let red = [1.0, 0.0, 0.0, 1.0];
    for e in window {
        if let Some(r) = e.render_args() {
            e.draw_2d(|c, g| {
            //loop{
                let mut is_draw_needed = cpu.execute(&mut memory);                
                if is_draw_needed {
                    clear([0.0; 4], g);
                    for y in (0..32){
                        ////println!("y - {}", y);
                        for x in (0..64){
                            ////println!("x - {}", x);
                            let color  = if cpu.gfx[((y*64) + x) as usize] == 0{
                              black 
                            }
                            else{
                               red 
                            };
                            rectangle(color,
                                      [x as f64 * 10.0 + 0.0 as f64, y as f64 * 10.0 + 0.0 as f64, 
                                       x as f64 * 10.0 + 10.0 as f64, y as f64 * 10.0 + 10.0 as f64],
                                      c.transform, g);
                            //break;
                        }
                    }
                    is_draw_needed = false;
                } 
            });
        }

        if let Some(b) = e.press_args(){
            match &b {
                &Button::Keyboard(k) => match &k {
                    &Key::Escape => std::process::exit(0),
                    &Key::D1 => cpu.keys[0x1] = 1 as u8,
                    &Key::D2 => cpu.keys[0x2] = 1 as u8,
                    &Key::D3 => cpu.keys[0x3] = 1 as u8,
                    &Key::D4 => cpu.keys[0xC] = 1 as u8,
                    &Key::Q => cpu.keys[0x4] = 1 as u8,
                    &Key::W => cpu.keys[0x5] = 1 as u8,
                    &Key::E => cpu.keys[0x6] = 1 as u8,
                    &Key::R => cpu.keys[0xD] = 1 as u8,
                    &Key::A => cpu.keys[0x7] = 1 as u8,
                    &Key::S => cpu.keys[0x8] = 1 as u8,
                    &Key::D => cpu.keys[0x9] = 1 as u8,
                    &Key::F => cpu.keys[0xE] = 1 as u8,
                    &Key::Z => cpu.keys[0xA] = 1 as u8,
                    &Key::X => cpu.keys[0x0] = 1 as u8,
                    &Key::C => cpu.keys[0xB] = 1 as u8,
                    &Key::V => cpu.keys[0xF] = 1 as u8,
                    
                    _ => {}
                },
                _ => {}
            }
        }

        if let Some(b) = e.release_args(){
            match &b {
                &Button::Keyboard(k) => match &k {
                    &Key::Escape => std::process::exit(0),
                    &Key::D1 => cpu.keys[0x1] = 0 as u8,
                    &Key::D2 => cpu.keys[0x2] = 0 as u8,
                    &Key::D3 => cpu.keys[0x3] = 0 as u8,
                    &Key::D4 => cpu.keys[0xC] = 0 as u8,
                    &Key::Q => cpu.keys[0x4] = 0 as u8,
                    &Key::W => cpu.keys[0x5] = 0 as u8,
                    &Key::E => cpu.keys[0x6] = 0 as u8,
                    &Key::R => cpu.keys[0xD] = 0 as u8,
                    &Key::A => cpu.keys[0x7] = 0 as u8,
                    &Key::S => cpu.keys[0x8] = 0 as u8,
                    &Key::D => cpu.keys[0x9] = 0 as u8,
                    &Key::F => cpu.keys[0xE] = 0 as u8,
                    &Key::Z => cpu.keys[0xA] = 0 as u8,
                    &Key::X => cpu.keys[0x0] = 0 as u8,
                    &Key::C => cpu.keys[0xB] = 0 as u8,
                    &Key::V => cpu.keys[0xF] = 0 as u8,
                    
                    _ => {}
                },
                _ => {}
            }
        }
    }
}
